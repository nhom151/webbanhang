<?php
namespace App\DesignPattern\Decorators;

interface DiscountInterface
{
    public function applyDiscount($total);
}
