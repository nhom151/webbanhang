<?php
namespace App\DesignPattern\Decorators;

use App\DesignPattern\Decorators\DiscountInterface;

class PercentDiscount implements DiscountInterface
{
    protected $percent;

    public function __construct($percent)
    {
        $this->percent = $percent;
    }

    public function applyDiscount($total)
    {
        return $total - ($total * $this->percent / 100);
    }
}
