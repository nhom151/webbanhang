<?php 
namespace App\DesignPattern\Observers;

use App\Models\Order;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderConfirmationEmail;

class OrderObserver
{
    public function created(Order $order)
    {
        Mail::to($order->user->email)
            ->send(new OrderConfirmationEmail($order));
    }
}
