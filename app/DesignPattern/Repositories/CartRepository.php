<?php

namespace App\DesignPattern\Repositories;

use App\DesignPattern\Repositories\CartRepositoryInterface;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Auth;

class CartRepository implements CartRepositoryInterface
{
    public function index()
    {
        return Session::get('cart', []);
    }

    public function add($product)
    {
        $cart = Session::get('cart', []);

        if (isset($cart[$product->id])) {
            // Nếu sản phẩm đã có trong giỏ hàng, tăng số lượng
            $cart[$product->id]['quantity']++;
        } else {
            // Nếu sản phẩm chưa có trong giỏ hàng, thêm mới
            $cart[$product->id] = [
                'name' => $product->name,
                'quantity' => 1,
                'price' => $product->price,
                'image' => $product->image,
            ];
        }
        // Cập nhật tổng giá cho sản phẩm dựa trên số lượng
        $cart[$product->id]['total'] = $cart[$product->id]['quantity'] * $product->price;
        Session::put('cart', $cart);
    }

    public function update($quantities)
    {
        $cart = Session::get('cart', []);
        foreach ($quantities as $productId => $quantity) {
            if (isset($cart[$productId])) {
                $cart[$productId]['quantity'] = $quantity;
                $cart[$productId]['total'] = $cart[$productId]['quantity'] * $cart[$productId]['price'];
            }
        }
        Session::put('cart', $cart);
    }

    public function remove($productId)
    {
        $cart = Session::get('cart', []);
        if (isset($cart[$productId])) {
            unset($cart[$productId]);
            Session::put('cart', $cart);
        }
    }

    public function confirmCheckout($address)
    {
        $cart = Session::get('cart', []);;
        DB::beginTransaction();
        try {
            $total = array_sum(array_map(function ($item) {
                return $item['price'] * $item['quantity'];
            }, $cart));

            // Tạo đơn hàng mới
            $order = new Order([
                'user_id' => Auth::id(),
                'status' => 'pending',
                'total_amount' => $total,
                'shipping_address' => $address, // Lưu địa chỉ giao hàng
            ]);
            $order->save();

            // Lưu chi tiết đơn hàng vào bảng order_items
            foreach ($cart as $productId => $item) {
                $orderItem = new OrderItem([
                    'order_id' => $order->id,
                    'product_id' => $productId,
                    'quantity' => $item['quantity'],
                    'price' => $item['price'],
                ]);
                $orderItem->save();
            }

            // Xóa giỏ hàng sau khi lưu đơn hàng
            session()->forget('cart');

            DB::commit();

            // Trả về ID đơn hàng
            return $order->id;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
