<?php
namespace App\DesignPattern\Repositories;

use Illuminate\Http\Request;

interface CartRepositoryInterface {
    public function index();
    public function add($product);
    public function update($quantities);
    public function remove($productId);
    public function confirmCheckout($address);
}