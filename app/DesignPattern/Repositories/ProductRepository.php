<?php
namespace App\DesignPattern\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;
use App\DesignPattern\Strategies\ProductSortStrategy;

class ProductRepository implements ProductRepositoryInterface
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            $data['image'] = $name;
        }
        return $this->model->create($data);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($request->hasFile('image')) {
            // Xóa ảnh cũ nếu tồn tại
            $oldProduct = $this->find($id);
            if($oldProduct->image) {
                $oldImagePath = public_path('/images/' . $oldProduct->image);
                if(file_exists($oldImagePath)) {
                    @unlink($oldImagePath);
                }
            }

            // Lưu ảnh mới
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            $data['image'] = $name;
        }
        $record = $this->find($id);
        return $record->update($data);
    }

    public function delete($id)
    {
        // Tìm sản phẩm
        $product = $this->find($id);

        // Xóa hình ảnh từ thư muc 'images'
        if($product->image) {
            $imagePath = public_path('/images/' . $product->image);
            if(file_exists($imagePath)) {
                @unlink($imagePath);
            }
        }

        // Xóa sản phẩm từ database
        return $this->model->destroy($id);
    }
    public function getAllProducts($sortMethod)
    {
        $strategy = ProductSortStrategy::create($sortMethod);
        return $this->model->orderBy($strategy->getColumn(), $strategy->getOrder())->get();
    }
}
