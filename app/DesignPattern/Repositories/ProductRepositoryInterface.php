<?php
namespace App\DesignPattern\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductRepositoryInterface
{
    public function all();

    public function find($id);

    public function create(Request $request);

    public function update(Request $request, $id);

    public function delete($id);
    public function getAllProducts($sortMethod); 
}