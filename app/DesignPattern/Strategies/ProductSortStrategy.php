<?php

namespace App\DesignPattern\Strategies;

class ProductSortStrategy
{
    protected $column;
    protected $order;

    public function __construct($column, $order)
    {
        $this->column = $column;
        $this->order = $order;
    }

    public static function create($sortMethod)
    {
        switch ($sortMethod) {
            case 'price_asc':
                return new self('price', 'asc');
            case 'price_desc':
                return new self('price', 'desc');
            case 'name':
                return new self('name', 'asc');
                case 'id':
                    return new self('id', 'desc');
            default:
                return new self('created_at', 'desc');
        }
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getOrder()
    {
        return $this->order;
    }
}
