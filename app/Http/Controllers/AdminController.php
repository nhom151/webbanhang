<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DesignPattern\Repositories\ProductRepositoryInterface;
use App\Models\Order;

class AdminController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $products = $this->productRepository->all();
        return view('admin.products.index', compact('products'));
    }
    public function orders()
    {
        $orders = Order::with(['user', 'orderItems.product'])->get();
        return view('admin.orders.index', compact('orders'));
    }
    public function dashboard()
    {
        return view('admin.dashboard');
    }
}
