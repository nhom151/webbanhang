<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ShippingMethod;
use App\DesignPattern\Repositories\CartRepositoryInterface;
use App\DesignPattern\Repositories\ProductRepositoryInterface;
use App\DesignPattern\Decorators\DiscountInterface;
use App\DesignPattern\Decorators\PercentDiscount;



class CartController extends Controller
{
    protected $productRepository;
    protected $cartRepository;

    public function __construct(ProductRepositoryInterface $productRepository, CartRepositoryInterface $cartRepository)
    {
        $this->productRepository = $productRepository;
        $this->cartRepository = $cartRepository;
    }

    public function index()
    {
        $cart = $this->cartRepository->index();
        return view('cart.index', compact('cart'));
    }


    public function add($productId)
    {
        $product = $this->productRepository->find($productId);
        if (!$product) {
            return redirect()->back()->with('error', 'Sản phẩm không tồn tại!');
        }

        $this->cartRepository->add($product);

        return redirect()->back()->with('success', 'Đã thêm sản phẩm vào giỏ hàng!');
    }

    public function update(Request $request)
    {
        $quantities = $request->input('quantity');
        $this->cartRepository->update($quantities);
        return redirect()->back()->with('success', 'Giỏ hàng đã được cập nhật!');
    }

    public function remove($productId)
    {
        $this->cartRepository->remove($productId);
        return redirect()->back()->with('success', 'Sản phẩm đã được xóa khỏi giỏ hàng!');
    }



    public function checkout(Request $request)
    {
        $cart = session('cart');
        if (!$cart) {
            return redirect()->route('cart.index')->with('error', 'Giỏ hàng của bạn đang trống!');
        }

        $total = array_sum(array_column($cart, 'total'));
        $shippingMethods = ShippingMethod::all();  // Lấy danh sách phương thức vận chuyển
        if ($request->has('apply_discount') && $request->has('discount_code')) {
            // Kiểm tra nếu mã giảm giá là 'giamgia10'
            if ($request->discount_code === 'giamgia10') {
                $discount = new PercentDiscount(10);  // Áp dụng giảm giá 10%
                $total = $discount->applyDiscount($total);
                session(['total_after_discount' => $total]);
            } else {
                return redirect()->route('cart.index.checkout')->with('error', 'Mã giảm giá không hợp lệ.');
            }
        }

        return view('cart.checkout', compact('cart', 'total', 'shippingMethods'));
    }
    public function showCheckout()
    {
        $cart = session('cart');
        if (!$cart) {
            return redirect()->route('cart.index')->with('error', 'Giỏ hàng của bạn đang trống!');
        }
        if (!$cart) {
            return redirect()->route('cart.index')->with('error', 'Giỏ hàng của bạn đang trống!');
        }

        $total = array_sum(array_column($cart, 'total'));
        $shippingMethods = ShippingMethod::all();

        return view('cart.checkout', compact('cart', 'total', 'shippingMethods'));
    }

    public function confirmCheckout(Request $request)
    {
        try {
            $address = $request->input('address'); // Lấy địa chỉ giao hàng từ form
            $this->cartRepository->confirmCheckout($address);
    
            // Redirect tới trang xác nhận đơn hàng
            return redirect()->route('order.success');
        } catch (\Exception $e) {
            return redirect()->route('cart.index.checkout')->with('error', 'Đã xảy ra lỗi khi xử lý đơn hàng của bạn: ' . $e->getMessage());
        }
    }
}
