<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DesignPattern\Repositories\ProductRepositoryInterface;

class ProductController extends Controller
{
    protected $product;

    public function __construct(ProductRepositoryInterface $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $products = $this->product->all();
        return view('admin.products.index', compact('products'));
    }
    public function home()
    {
        $products = $this->product->all();
        return view('home', compact('products'));
    }
    public function show($id)
    {
        $product = $this->product->find($id);
        $products = $this->product->all();
        if (!$product) {
            return redirect('/products')->with('error', 'Product not found');
        }

        return view('product.product', compact('product', 'products'));
    }
    public function sort($method)
    {
        $products = $this->product->getAllProducts($method);
        return view('home', compact('products'));
    }
    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $this->product->create($request);
        return redirect()->route('admin.products.index');
    }

    public function edit($id)
    {
        $product = $this->product->find($id);
        return view('admin.products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $this->product->update($request, $id);
        return redirect()->route('admin.products.index');
    }

    public function destroy($id)
    {
        $this->product->delete($id);
        return redirect()->route('admin.products.index');
        }
}
