<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'status', 'total_amount'];

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    // Định nghĩa mối quan hệ với User 
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getTotalAmountAttribute()
    {
        return $this->orderItems->sum(function ($orderItem) {
            return $orderItem->quantity * $orderItem->price;
        });
    }
}
