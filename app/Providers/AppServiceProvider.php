<?php

namespace App\Providers;

use App\DesignPattern\Observers\OrderObserver;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use App\DesignPattern\Observers\UserObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Order::observe(OrderObserver::class);
    }
}
