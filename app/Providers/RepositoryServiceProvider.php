<?php 
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\DesignPattern\Repositories\ProductRepositoryInterface;
use App\DesignPattern\Repositories\ProductRepository;
use App\DesignPattern\Repositories\CartRepositoryInterface;
use App\DesignPattern\Repositories\CartRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class
        );
        $this->app->bind(
            CartRepositoryInterface::class, 
            CartRepository::class);
    }
}