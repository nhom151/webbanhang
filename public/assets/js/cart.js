function changeQuantity(change) {
    let quantityInput = document.getElementById('qtyInput');
    let currentQuantity = parseInt(quantityInput.value);
    let newQuantity = currentQuantity + change;
    
    // Ensure quantity stays within bounds
    newQuantity = Math.max(1, Math.min(newQuantity, 20));
    quantityInput.value = newQuantity;
    document.getElementById('formQuantity').value = newQuantity;
    
    // Update total price
    let pricePerItem = parseFloat(document.getElementById('totalPrice').getAttribute('data-price'));
    document.getElementById('totalPrice').innerText = '$' + (pricePerItem * newQuantity).toFixed(2);
}

document.getElementById('qtyInput').addEventListener('change', function() {
    changeQuantity(0); // Recalculate total when user manually changes quantity
});