<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quản lý Admin</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin-dashboard.css') }}">
</head>
<body>
    <div class="container">
        <h1>Trang quản lý Admin</h1>
        <div class="navigation">
            <a href="{{ route('admin.products.index') }}" class="btn btn-primary">Quản lý Sản Phẩm</a>
            <a href="{{ route('admin.orders.index') }}" class="btn btn-secondary">Quản lý Hóa Đơn</a>
        </div>
    </div>
</body>
</html>
