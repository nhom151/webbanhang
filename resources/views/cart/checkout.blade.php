<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thanh toán</title>
    <!-- Thêm Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .total-amount {
            font-weight: bold;
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <h2 class="text-center">Thanh toán</h2>
        <form method="post" action="{{ route('cart.checkout') }}">
            @csrf
            <div class="form-group">
                <label for="discount_code">Mã giảm giá:</label>
                <input type="text" class="form-control" id="discount_code" name="discount_code" placeholder="Nhập mã giảm giá nếu có">
                <button type="submit" name="apply_discount" class="btn btn-info">Áp dụng giảm giá</button>
                @if (session('error'))
                <div class="alert alert-danger mt-2">
                    {{ session('error') }}
                </div>
                @endif
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Giá</th>
                            <th>Tổng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cart as $id => $details)
                        <tr>
                            <td>{{ $details['name'] }}</td>
                            <td>{{ $details['quantity'] }}</td>
                            <td>{{ $details['price'] }}</td>
                            <td>{{ $details['total'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <p class="total-amount">Tổng tiền: ${{ number_format($total, 2) }} </p>

            <div class="form-group">
                <label for="shipping_method" class="form-label">Phương thức vận chuyển:</label>
                <select id="shipping_method" name="shipping_method" class="form-control">
                    @foreach ($shippingMethods as $method)
                    <option value="{{ $method->id }}">{{ $method->name }} - ${{ number_format($method->price, 2) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="address" class="form-label">Địa chỉ giao hàng</label>
                <input type="text" class="form-control" id="address" name="address" required>
            </div>

            <div class="form-group">
                <label for="payment_method" class="form-label">Phương thức thanh toán: </label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="payment_method" id="cash" value="cash" checked>
                    <label class="form-check-label" for="cash">Tiền mặt</label>
                </div>
            </div>

            

    </div>
    </form>
    <form action="{{ route('cart.confirm.checkout') }}" method="POST">
                @csrf
                <div class="text-right">
                    <button type="submit" class="btn btn-success">Hoàn tất thanh toán</button>
                </div>
            </form>
    <!-- Thêm Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
</body>

</html>