@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Giỏ hàng của bạn</h2>
    <form action="{{ route('cart.update') }}" method="POST">
        @csrf
        @if(session('cart'))
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Hình ảnh</th>
                        <th>Tên sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Tổng</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(session('cart') as $id => $details)
                        <tr>
                            <td><img src="{{ asset('images/' . $details['image']) }}" width="100" height="100"></td>
                            <td>{{ $details['name'] }}</td>
                            <td>
                                <input type="number" name="quantity[{{ $id }}]" value="{{ $details['quantity'] }}">
                            </td>
                            <td>{{ $details['price'] }}</td>
                            <td>{{ $details['total'] }}</td>
                            <td>
                            <form action="{{ route('cart.remove', $id) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger">Xóa</button>
                            </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right"><strong>Tổng cộng</strong></td>
                        <td>{{ array_sum(array_column(session('cart'), 'total')) }}</td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
            <div class="text-right">
                <button type="submit" class="btn btn-info">Cập nhật</button>
                <form action="{{ route('cart.checkout') }}" method="POST" style="display: inline;">
                    @csrf
                    <button type="submit" class="btn btn-primary">Thanh toán</button>
                </form>
            </div>
        @else
            <p>Giỏ hàng của bạn đang trống!</p>
        @endif
    </form>
</div>
@endsection
