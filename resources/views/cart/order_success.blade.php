<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thanh toán</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/order-success.css') }}">
</head>

<body>
    <div class="container">
        <div class="alert alert-success" role="alert">
            Đơn hàng của bạn đã được đặt thành công, vui lòng chờ 2-3 ngày để nhận hàng.
        </div>
        <a href="{{ route('home') }}" class="btn btn-primary">Trở về trang chủ</a>
    </div>
</body>

</html>