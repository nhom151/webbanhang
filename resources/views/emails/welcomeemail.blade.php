<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Fruit Shop</title>
</head>
<body>
    <h1>Chào mừng {{ $user->name }} đến với trang web bán trái cây!</h1>
    <p>Cảm ơn bạn đã đăng ký tài khoản tại trang web bán trái cây. Chúng tôi rất vui mừng khi được chào đón bạn!</p>
    <!-- Bất kỳ thông tin hoặc liên kết nào khác bạn muốn bao gồm trong email chào mừng cũng có thể được thêm vào đây -->
</body>
</html>
