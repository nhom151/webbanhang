@extends('layouts.app')

@section('content')
<a id="sort" href="{{ route('products.sort', ['method' => 'default']) }}">Mặc định</a>
<a id="sort" href="{{ route('products.sort', ['method' => 'price_asc']) }}">Giá: Thấp đến Cao</a>
<a id="sort" href="{{ route('products.sort', ['method' => 'price_desc']) }}">Giá: Cao đến Thấp</a>
<a id="sort" href="{{ route('products.sort', ['method' => 'name']) }}">Tên</a>
<a id="sort" href="{{ route('products.sort', ['method' => 'id']) }}">Mới nhất</a>

<!--Block 03: Product Tab-->
<div class="product-tab z-index-20 sm-margin-top-193px xs-margin-top-30px">
    <div class="container">
        <div class="biolife-tab biolife-tab-contain sm-margin-top-34px">
            <div class="tab-content">
                <div id="tab01_1st" class="tab-contain active">
                    <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile eq-height-contain" data-slick='{"rows":2 ,"arrows":true,"dots":false,"infinite":true,"speed":400,"slidesMargin":10,"slidesToShow":4, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 4}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":25 }},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":15}}]}'>
                        @foreach ($products as $product)
                        <li class="product-item">
                            <div class="contain-product layout-default">
                                <div class="product-thumb">
                                    <a href="{{ url('/products/' . $product->id) }}" class="link-to-product">
                                        <img src="{{ asset('images/' . $product->image) }}" alt="{{ $product->name }}" width="270" height="270" class="product-thumnail">
                                    </a>
                                </div>
                                <div class="info">
                                    <h4 class="product-title"><a href="{{ url('/products/' . $product->id) }}" class="pr-name">{{ $product->name }}</a></h4>
                                    <div class="price ">
                                        <ins><span class="price-amount"><span class="currencySymbol">$</span>{{ $product->price }}</span></ins>
                                    </div>
                                    <div class="slide-down-box">
                                        <p class="message">{{ $product->description }}</p>
                                        <form action="{{ route('cart.add', ['productId' => $product->id]) }}" method="POST">
                                            @csrf
                                            <div class="buttons">
                                                <button type="submit" class="btn add-to-cart-btn">Thêm vào giỏ hàng</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endsection